<?php
    ini_set('display_errors',1);
    error_reporting(E_ALL|E_STRICT);
    
    include('classes/questions/constituency.php');
    include('classes/questions/voting.php');
    include('classes/questions/party.php');
    
    include('classes/vote.php');
    list($voteSuccess, $voteMessage) = Vote::voteSubmitted();
?>

<html>
    <head>
        <title>Election Quiz</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
    </head>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Election Quiz</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/">Questions</a></li>
            <li><a href="/results.php">Results</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container" style="margin-top: 60px;">
        <div class="text-center">
            <h1>Questions</h1>
            
            <?php if (!empty($voteMessage)) : ?>
                <?php $alertStyle = $voteSuccess ? 'success' : 'warning';?>
                <div class="alert alert-<?= $alertStyle; ?>">
                    <?= $voteMessage; ?>.
                </div>
            <?php endif; ?>
            
            <?php if (!$voteSuccess) : ?>
                <form class="form-horizontal" method="post" action="index.php">
                    <?php new Constituency();?>
                    <?php new Voting();?>
                    <?php new Party();?>

                    <div class="form-group">
                        <div class="col-sm-offset-1 col-sm-11">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            <?php endif; ?>
        </div>
    </div><!-- /.container -->
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
        $("#Question_party").hide();
        $("#Question_select_voting").change(function(){
            if ($('#Question_select_voting').val() == 1) {
                $("#Question_party").show();
            } else {
                $("#Question_party").hide();
            }
        });
    </script> 
  </body>
</html>