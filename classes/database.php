<?php

class Database
{
    private $host = '127.0.0.1';
    private $db = 'kineo';
    private $user = 'root';
    private $pass = 'xxxxx';
    private $charset = 'utf8';
    
    public function connect()
    {
        $db = "mysql:host={$this->host};dbname={$this->db};charset={$this->charset}";
        return new PDO($db, $this->user, $this->pass);
    }
}
