<?php

require_once('database.php');

class Vote
{
    static public function voteSubmitted()
    {
        if (isset($_POST['Question'])) {
            $data = $_POST['Question'];
            if ($data['constituency'] === '' || is_null($data['constituency'])) {
                return [false, 'Constituency must be set'];
            } elseif ($data['voting'] === '' || is_null($data['voting'])) {
                return [false, 'You must select whether your are not voting'];
            } elseif (!empty($data['voting']) && $data['party'] === '') {
                return [false, 'You must select the party you are voting form'];
            }

            $db = new Database;
            $pdo = $db->connect();
            $stmt = $pdo->prepare("INSERT INTO results "
                    . "(constituency_id, voting, party_id) "
                    . "VALUES (:constituency, :voting, :party)");
            $stmt->bindParam(':constituency', $data['constituency'], PDO::PARAM_INT);
            $stmt->bindParam(':voting', $data['voting'], PDO::PARAM_INT);
            if (empty($data['voting'])) {
                $stmt->bindValue(':party', null, PDO::PARAM_INT);
            } else {
                $stmt->bindParam(':party', $data['party'], PDO::PARAM_INT);
            }
            
            if ($stmt->execute()) {
                return [true, 'Successfully submitted vote'];
            } else {
                return [false, 'There was an error saving your vote'];
            }
        }
        
        return [false, ''];
    }
}