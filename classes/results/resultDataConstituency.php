<?php

require_once('resultData.php');

class ResultDataConstituency extends ResultData
{
    public function getData()
    {
        $db = $this->getDbConnection();
        return $db->query('SELECt count(*) as count, party.name as party, c.name as constituency FROM results '
                . 'left join party on party.party_id = results.party_id '
                . 'left join constituency c on c.constituency_id = results.constituency_id '
                . 'group by c.constituency_id, party.party_id')
                ->fetchAll();
    }
}