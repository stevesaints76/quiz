<?php

require_once('resultData.php');

class ResultDataNational extends ResultData
{
    public function getData()
    {
        $db = $this->getDbConnection();
        return $db->query('SELECt count(*) as count, party.name as party FROM results '
                . 'left join party on party.party_id = results.party_id '
                . 'group by party.party_id')
                ->fetchAll();
    }
}