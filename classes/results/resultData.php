<?php

require_once('classes/database.php');

abstract class ResultData
{
    abstract public function getData();
    
    protected function getDbConnection()
    {
        $db = new Database();
        return $db->connect();
    }
}