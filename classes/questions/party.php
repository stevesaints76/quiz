<?php

require_once('question.php');

class Party extends Question
{
    public function getName()
    {
        return 'party';
    }
    
    public function getQuestion()
    {
        return "Which party are you voting for?";
    }
    
    public function getAnswers()
    {
        $db = $this->getDbConnection();
        $results = $db
                ->query('select * from party ORDER BY name')
                ->fetchAll();
        
        $return = [];
        foreach ($results as $result) {
            $return[$result['party_id']] = $result['name'];
        }
        
        return $return;
    }
}