<div class="form-group" id="Question_<?= $name; ?>">
    <label class="control-label col-sm-6" for="<?= "Question[{$name}]"; ?>"><?= $question;?></label>

    <div class="col-sm-1">
        <select name="Question[<?= $name; ?>]" id="Question_select_<?= $name; ?>">
            <option value="">Please Select--</option>
            <?php foreach ($answers as $key => $value) : ?>
                <option value="<?= $key; ?>"><?= $value; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>
