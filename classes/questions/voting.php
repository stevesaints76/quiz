<?php

require_once('question.php');

class Voting extends Question
{
    public function getName()
    {
        return 'voting';
    }
    
    public function getQuestion()
    {
        return "Are you going to vote?";
    }
    
    public function getAnswers()
    {
        return ['1' => 'Yes', '0' => 'No'];
    }
}