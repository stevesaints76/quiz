<?php

require_once('classes/database.php');

abstract class Question
{
    abstract public function getName();
    abstract public function getQuestion();
    abstract public function getAnswers();
   
    public function __construct()
    {
        extract([
            'name' => $this->getName(),
            'question' => $this->getQuestion(), 
            'answers' => $this->getAnswers()
        ]);
        require('template.php');
    }
    
    protected function getDbConnection()
    {
        $db = new Database();
        return $db->connect();
    }
}