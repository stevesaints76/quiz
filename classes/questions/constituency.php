<?php

require_once('question.php');

class Constituency extends Question
{
    public function getName()
    {
        return 'constituency';
    }
    
    public function getQuestion()
    {
        return "Please select your constituency?";
    }
    
    public function getAnswers()
    {
        $db = $this->getDbConnection();
        $results = $db
                ->query('select * from constituency ORDER BY name')
                ->fetchAll();
        
        $return = [];
        foreach ($results as $result) {
            $return[$result['constituency_id']] = $result['name'];
        }
        
        return $return;
    }
}