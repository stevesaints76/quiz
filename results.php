<?php
    ini_set('display_errors',1);
    error_reporting(E_ALL|E_STRICT);
    
    include('classes/results/resultDataNational.php');
    include('classes/results/resultDataConstituency.php');
?>

<html>
    <head>
        <title>Election Quiz</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
    </head>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Election Quiz</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="/">Questions</a></li>
            <li class="active"><a href="/results.php">Results</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container" style="margin-top: 60px;">
        <div class="text-center">
            <h1>Results</h1>
        </div>
        <div>
            <h2>National Data</h2>
            <?php $results = new ResultDataNational();?>
            <div class="col-md-12">
                <?php if (empty($results->getData())) : ?>
                    <em>No results stored</em>
                <?php else : ?>
                    <?php foreach ($results->getData() as $data) : ?>
                        <?= empty($party = $data['party']) ? 'Not Voting' : $party; ?>: <?= $data['count']; ?><br/>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        
        <br/>
        
        <h2>Constituency Data</h2>
            <?php $results = new ResultDataConstituency();?>
            <div class="col-md-12">
                <?php if (empty($results->getData())) : ?>
                    <em>No results stored</em>
                <?php else : ?>
                    <?php foreach ($results->getData() as $data) : ?>
                        <?= $data['constituency']; ?>, <?= empty($party = $data['party']) ? 'Not Voting' : $party; ?>: <?= $data['count']; ?><br/>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>

    </div><!-- /.container -->
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>